package com.solution;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

/**
 Asssumptions:
 1) Written as a standalone java program
 - No external libraries
 - No logging framework.
 - Not using any distributed processing for this exercise.
 - Not using any multi-threaded / concurrent logic right now
 - It is definitely a room for improvement for PRODUCTIONALIZING this.
 - Used treeset for alphabetic listing of dups.

 2) No Unit Tests Given
 - Can be run by FilesApp.main method.
 - "<PUT YOUR PATH HERE>" placeholder can be replaced with the root foder path

 3) I assume happy path for now for this
 - Similar to a POC or Prototype.
 - Some validation/checks are done... But, not doing 'n'th level validation etc.
 - Also, currently NOT handling if a folder at a higher level is available at a lower level (could result in cyclical scenario)
 - as I have focussed mainly on the approach to identify the dups.

 4) Limitations
 - In reality, the filesystem can be 'n' level deep. May not be able to build all file-entries in Map
 (especially in a low RAM, high HDD systems).
 - File System may be in a constant state of flux.
 - Program assumes a 'snapshot' builder. Any change to underlying file (deleted, added etc) may not be conisidered for comparison.
 - A high level of file system recursion may require a higher stack size for the Java program.
 - Large files take large time to compare as well - to know the true duplicate

 Design and Approach :-

 1) Optimization 1: Only files are are same length (size) are compared and checked for duplicates.
 2) Optimization 2: Use a combination of file size, canonical path and the file content to determine duplicates.
 Step 1: runs snapshot builder to build file system entries.
 1) Group files by the file size...
 - Recursively query starting at the root path and go down.
 - Add each files to the map based on their size.
 - All files with same file size will be grouped under that size as a 'list' of files.
 - for ex.,
 Map
 <long size, [list of files]>
 { 1000, List [file1, file2, file7] } - possible duplicates
 { 1500, List [file3] } - no duplicates

 Step 2:
 1) From each size group compare the list of files.
 - Each size can be compared separately in a different thread (Not Implemented - But a future Improvement - for PROD)
 2) Canonical comparisons (takes care of physical, soft/hard link files) are considered.
 2) [Optimization] Comparison is efficient (Each iteration compares (n-ith) items only)
 - File System could have 1000s of files but only the files of 'same size' are compared with each other.
 - for 'n' files, each iteration will compare (n-1), (n-2) items only... and the last but one iteration will compare just 1 item) reducing the complexity.

 Expected Inputs/Outputs: replace the "<PUT YOUR PATH HERE>" inside the FilesApp.main method with a 'root' folder path.

 Summary on how to run the test client and its pre-requisites
 1) run the FilesApp.main method.

 Thanks.
 *
 */
public class FilesApp {

    private Set<String> dupsSet = new TreeSet<String>();
    private SnapshotFilesBuilder snapshotFilesBuilder = new SnapshotFilesBuilder();

    public FilesApp analyzeFiles(final String path) throws Exception {
        long start = System.currentTimeMillis();
        System.out.println("Building files...");
        snapshotFilesBuilder.buildFiles(path);
        System.out.println("Building files complete...");
        Map<Long, List<String>> filesToCompareMap = snapshotFilesBuilder.getFilesMap();
        System.out.println("Before compare size=" + filesToCompareMap.size());
        if (filesToCompareMap!=null) {
            for (List<String> files : filesToCompareMap.values()) {
                int size = files.size();
                if (size>1) {
                    for (int i=0;i<size-1;i++) {
                        for (int j=i+1;j<size;j++) {
                            File f1 = new File(files.get(i));
                            File f2 = new File(files.get(j));
                            System.out.println("Comparing files f1=" + f1.toString() + " f2=" + f2.toString());
                            if (FileCompareUtil.areFilesSame(f1, f2)) {
                                dupsSet.add(files.get(i));
                                dupsSet.add(files.get(j));
                            }
                            System.out.println("Comparing files Complete f1=" + f1.toString() + " f2=" + f2.toString());
                        }
                    }
                }
            }
        }
        System.out.println("After compare in " + (System.currentTimeMillis()-start) + "ms dups Size=" + dupsSet.size() );
        return this;
    }

    public FilesApp printDups() {
        for (String file : dupsSet) {
            System.out.println(file + " is a duplicate");
        }
        return this;
    }

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main( String[] args ) throws Exception {
        new FilesApp().analyzeFiles("C:\\home\\development\\projects\\java\\FilesApp\\data").printDups();
    }
}

class SnapshotFilesBuilder {
    private Map<Long, List<String>> filesMap = null;

    public SnapshotFilesBuilder() {
        filesMap = new HashMap<Long, List<String>>();
    }

    public void buildFiles(final String path) throws Exception {
        this.buildFilesList(path);
    }

    /**
     *
     * @param path
     * @throws Exception
     */
    protected void buildFilesList(final String path) throws Exception {
        File file = new File(path) ;
        File[] files = file.listFiles();
        if (files==null) return;
        for (File f: files) {
            if (f.isDirectory()) {
                buildFilesList(f.getAbsolutePath());
            } else {
                if (!filesMap.containsKey(f.length())) {
                    List<String> fileSet = new ArrayList<String>();
                    fileSet.add(f.getAbsolutePath());
                    filesMap.put(f.length(), fileSet);
                } else {
                    filesMap.get(f.length()).add(f.getAbsolutePath());
                }
            }
        }
    }

    public Map<Long, List<String>> getFilesMap() {
        return filesMap;
    }
}

class FileCompareUtil {

    /**
     *
     * @param f1
     * @param f2
     * @return
     */
    public static boolean areFilesSame(final File f1, final File f2) throws Exception {

        if (f1==null) return false;
        if (f2==null) return false;

        if (!f1.exists()) return false;
        if (!f2.exists()) return false;

        if (f1==f2) return true;

        // same files...
        if (f1.getCanonicalPath() == f2.getCanonicalPath()) {
            return true;
        }

        // compare length
        if (f1.length() != f2.length()) {
            return false;
        } else {
            if (hasSameContent(f1, f2)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param f1
     * @param f2
     * @return
     * @throws Exception
     */
    public static boolean hasSameContent(final File f1, final File f2) throws Exception {
        InputStream is1 = null;
        InputStream is2 = null;
        try {
            is1 = new BufferedInputStream(new FileInputStream(f1));
            is2 = new BufferedInputStream(new FileInputStream(f2));
            int c = -1;
            while ((c = is1.read()) != -1) {
                if (c != is2.read()) return false;
            }
            return (is2.read() == -1) ? true : false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (is1 != null) is1.close();
                if (is2 != null) is2.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
